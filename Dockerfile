#FROM ubuntu:16.04

#RUN apt update && apt -y install xrdp mate-core mate-desktop-environment mate-notification-daemon fonts-noto-cjk

#RUN useradd -m -d /home/rio -p rio rio
#RUN echo 'rio:docker' |chpasswd
#RUN adduser rio sudo

#EXPOSE 3389

FROM ubuntu:16.04

RUN apt update && apt -y install xrdp

RUN useradd -mp docker -s /bin/bash -G sudo nio

#RUN echo 'root:root' |chpasswd

EXPOSE 3389

CMD ["service", "xrdp", "start"]